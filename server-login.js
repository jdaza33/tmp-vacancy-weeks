/**
 *
 */

require("dotenv").config();
const path = require("path");
const express = require("express");
const history = require("connect-history-api-fallback");

const app = express();
const port = process.env.LOGIN_PORT || 30001;
const publicPath = path.resolve(__dirname, "./login");

const staticConf = { maxAge: "1y", etag: false };

app.use(express.static(publicPath, staticConf));
app.use(history());
app.use(express.static(publicPath, staticConf));

app.get("/", function (req, res) {
  res.render(path.join(__dirname, "index.html"));
});

app.listen(port, () => {
  console.log(`App running on port ${port}!`);
});
