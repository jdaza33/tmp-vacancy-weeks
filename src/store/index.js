import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

// Create a new store instance.
const store = new Vuex.Store({
  state() {
    return {
      resortList: [],
      filters: null,
      resort: null,
    }
  },
  mutations: {
    setResortList(state, list) {
      state.resortList = list
    },
    setFilter(state, data) {
      state.filters = data
    },
    setResort(state, resort) {
      state.resort = resort
    }
  }
})

export default store