import Vue from 'vue'
import VueI18n from 'vue-i18n'

import es from './es'
import en from './en'

Vue.use(VueI18n)

const messages = {
  es,
  en,
}

const i18n = new VueI18n({
  locale: localStorage.getItem('locale') || 'es', // set locale
  fallbackLocale: 'es', // set fallback locale
  messages // set locale messages
})

export default i18n
