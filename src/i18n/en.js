const translate = {
    buttons: {
        helpme: "Help me",
        es: "Spanish",
        en: "English",
        commentsAndSupport: "Comments/Support",
        login: 'Login',
        logout: "Logout",
        myProfile: "My Profile",
        lodging: "Loging",
        hotelMostSold: "Hotels most sold",
        search: 'Search',
        select: 'Select...',
        next: 'Next',
        forgotPassword: 'Forgot your password?',
        rememberPassword: 'Remember password',
        rooms: 'Rooms',
        destiny: 'Destiny',
        seeOption: 'See options'
    },
    tabs: {
        weeks: 'Weeks',
        hotels: 'Hotels',
        vacationsPacks: 'Vacations Packs',
        flights: 'Flights',
        flightHotel: 'Flight + Hotel',
        activities: 'Activities',
        rentACar: 'Rena A Card',
        multipleDestination: 'Multiples destinatios',
        transfer: 'Transfer',
        weekend: 'Weekend'
    },
    forms: {
        country: 'Country',
        countrySelected: 'Select a country',
        city: 'City',
        state: 'State',
        stateSelected: 'Select a state',
        checkIn: 'Check In',
        checkOut: 'Check Out',
        allCities: 'All Cities',
        resortName: 'Resort Name',
        userOrEmail: 'User or Email',
        password: 'Password',
        totalPrice: 'Total price',
        nights: 'Nights',
        changeYourTrip: 'Change your trip',
        propertyName: 'Property Name',
        selectYourAccommodation: 'Select your accommodation in',
        amongXResult: 'Among {result} Hotels'
    },
    message: {
        noDataText: 'No Data Found',
        accommodationIn: 'Accommodation in',
    }

}

export default translate