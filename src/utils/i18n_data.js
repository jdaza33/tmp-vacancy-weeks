module.exports = {
  welcome: {
    es: "Bienvenido",
    en: "Welcome",
  },
  loginToContinue: {
    es: "Inicia sesión para continuar",
    en: "Log in to continue",
  },
  spanish: {
    es: "Español",
    en: "Spanish",
  },
  english: {
    es: "Ingles",
    en: "English",
  },
  loginIdOrEmail: {
    es: "LoginID o Correo electrónico",
    en: "LoginID or Email",
  },
  password: {
    es: "Contraseña",
    en: "Password",
  },
  login: {
    es: "Iniciar sesión",
    en: "LogIn",
  },
  forgotYourPassword: {
    es: "¿Olvidaste tu contraseña?",
    en: "Forgot your password?",
  },
  socialNetworks: {
    es: "Redes sociales",
    en: "Social networks",
  },
  contact: {
    es: "Contacto",
    en: "Contact",
  },
  cookiesPolicy: {
    es: "Política de cookies",
    en: "Cookies policy",
  },
  privacyPolicy: {
    es: "Política de privacidad",
    en: "Privacy Policy",
  },
  weeks: {
    es: "Semanas",
    en: "Weeks",
  },
  country: {
    es: "País",
    en: "Country",
  },
  countrySelected: {
    es: "Selecciona un país",
    en: "Select a country",
  },
  state: {
    es: "Estado",
    en: "State",
  },
  stateSelected: {
    es: "Selecciona un estado",
    en: "Select a state",
  },
  city: {
    es: "Ciudad",
    en: "City",
  },
  citySelected: {
    es: "Selecciona una ciudad",
    en: "Select a city",
  },
  checkIn: {
    es: "Check In",
    en: "Check In",
  },
  checkOut: {
    es: "Check Out",
    en: "Check Out",
  },
  totalPrice: {
    es: "Precio Total",
    en: "Total price",
  },
  moreDetails: {
    es: "Más detalles",
    en: "More details",
  },
  nonRefundable: {
    es: "No reembolsable",
    en: "Non Refundable",
  },
  agency: {
    es: "Agencia",
    en: "Agency",
  },
  interestedLinks: {
    es: "Enlaces de interés",
    en: "Interest links",
  },
  commentsSupport: {
    es: "Comentarios/Soporte",
    en: "Comments/Support",
  },
  hotelsFlightsMore: {
    es: "Hoteles, vuelos y más ...",
    en: "Hotels, flights and more ...",
  },
  myProfile: {
    es: "Mi perfil",
    en: "My profile",
  },
  find: {
    es: "Buscar",
    en: "Search",
  },
  nights: {
    es: "Noches",
    en: "Nights",
  },
  pay: {
    es: "Pagar",
    en: "Pay",
  },
  morePictures: {
    es: "Más fotos",
    en: "More pictures",
  },
  aboutProperty: {
    es: "Sobre la Propiedad",
    en: "About the Property",
  },
  services: {
    es: "Servicios",
    en: "Services",
  },
  policies: {
    es: "Políticas",
    es: "Policies",
  },
  importantInformation: {
    es: "Información Importante",
    en: "Important Information",
  },
  payNow: {
    es: "Pagar ahora",
    en: "Pay now",
  },
  successfulPayment: {
    es: "Pago exitoso",
    en: "Successful payment",
  },
  reserving: {
    es: "Reservando",
    en: "Reserving",
  },
  startingStripe: {
    es: "Iniciando Stripe",
    en: "Starting Stripe",
  },
  all: {
    es: "Todos",
    en: "All",
  },
  weekType: {
    es: "Tipo de semana",
    en: "Week Type",
  },
};
